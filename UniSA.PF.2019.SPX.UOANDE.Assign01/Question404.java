package group04ArrayManipulation;


/**
 * Write a static method called removeOddNumbers that goes through a list of integers 
 * and removes all odd value and returns a new list with just the even numbers.  The list of results 
 * should only be as long as necessary so assume.
 * 
 * 
 * @author YOUREMAILIDHERE   
 * This is mandatory otherwise you may get zero for the question
 *
 */
public class Question404 {
  public static void main(String[] args) {
    
    int[] a = {1,2,1,3,4,4,5,8,1,2,5,0,3,4,5,3,2,1,2,3,4,1,1,1,1,1,11,1000};
    int size = 7;
    int[] result = removeOddNumbers(a);
    for (int i=0;i<result.length;i++) {
      System.out.println(i+"->"+result[i]);
    }

    /*
          outputs
0->2
1->4
2->4
3->8
4->2
5->0
6->4
7->2
8->2
9->4
10->1000
     */
  }


  //Write your solution in this method
  public static int[] removeOddNumbers(int[] a) {

    
    
    
    
}
